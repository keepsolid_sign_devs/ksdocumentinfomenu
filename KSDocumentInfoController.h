#ifndef KSDOCUMENTINFOCONTROLLER_H
#define KSDOCUMENTINFOCONTROLLER_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <KSFinishEditDocController.h>

class KSDocumentInfoController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isMenuOpened MEMBER _isMenuOpened NOTIFY menuOpenedChanged);
    Q_PROPERTY(KSDocumentInfoButton hintStep MEMBER _hintStepIndex NOTIFY hintStepChanged);
    Q_PROPERTY(KSDocumentInfoButton openedStep MEMBER _openedStepIndex NOTIFY openedStepChanged);
    Q_PROPERTY(KSDocumentInfoMenuState menuState MEMBER _menuState NOTIFY menuStateChanged);
public:

    explicit KSDocumentInfoController(QObject *parent = nullptr);
    ~KSDocumentInfoController();
    enum KSDocumentInfoButton: int {

        NotChoosen = 0,
        Participants,
        Annotations,
        Finish,
        Send
    };
    Q_ENUMS(KSDocumentInfoButton);

    enum KSDocumentInfoMenuState: int {

        Prepeared = 0,
        Signing,
        Signed
    };
    Q_ENUMS(KSDocumentInfoMenuState);

    Q_INVOKABLE void stepButtonClicked(KSDocumentInfoButton index);
    Q_INVOKABLE void incrementMenuState();
    Q_INVOKABLE void incrementHintIndex();

signals:

    void menuOpenedChanged();
    void hintStepChanged();
    void openedStepChanged();
    void menuStateChanged();

    void annotationsMenuOpened();
    void participantsMenuOpened();
    void finishMenuOpened();

public slots:


private:
    bool _isMenuOpened = false;
    KSDocumentInfoButton _hintStepIndex = NotChoosen;
    KSDocumentInfoButton _openedStepIndex = NotChoosen;
    KSDocumentInfoMenuState _menuState = Prepeared;
};

#endif // KSDOCUMENTINFOCONTROLLER_H
