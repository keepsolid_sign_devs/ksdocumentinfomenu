#include "KSFinishEditDocController.h"
#include <QQuickItem>

KSFinishEditDocController::KSFinishEditDocController(QObject* _parent) : QObject(_parent)
{
    fillData();
}

KSFinishEditDocController::~KSFinishEditDocController(){
}

void KSFinishEditDocController::fillData(){
    m_doc_model.setName("Some dummy document");
    m_doc_model.setMessage("For one thing they usually step all over the hedges and plants on the side of someone’s house killing them and setting back the vegetation’s gardener countless time and effort.");
    m_doc_model.setHasOrder(false);
    m_doc_model.setCreationDate(QDate::currentDate());

    long id = 1;
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255),0,5);
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255),0,4);
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255), 0,3);
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255), 0,2);
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255), 1,2);
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255), 1,2);
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255), 1,2);
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
    contactsModel.addContact(id++, "John", "Doe", "example@gmail.com", "KeepSolid", "", QColor::fromRgb(rand() % 255, rand() % 255, rand() % 255));
}

void KSFinishEditDocController::displayInitialState(){
    qDebug()<<"displayInitialState";
    QVariant docModel;
    docModel.setValue(&m_doc_model);
    emit displayDocModel(docModel);

    QVariant m_contactsModel;
    m_contactsModel.setValue(&contactsModel);
    emit displayParticipants(m_contactsModel);
}

void KSFinishEditDocController::onDocumentNameChanged(QString newName){
    qDebug()<<"onDocumentNameChanged "<<newName;
    m_doc_model.setName(newName);
}

void KSFinishEditDocController::onDocumentMessageChanged(QString newMessage){
    qDebug()<<"onDocumentMessageChanged "<<newMessage;
    m_doc_model.setMessage(newMessage);
}
