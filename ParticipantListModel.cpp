#include <QHash>
#include "ParticipantListModel.h"

ParticipantListModel::ParticipantListModel(QObject *parent)
    : QAbstractListModel(parent)
{

}

ParticipantListModel::~ParticipantListModel()
{
    for(int i = 0; i < dataList.count(); i++) {
        delete dataList.at(i);
    }
}

QHash<int, QByteArray> ParticipantListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[FirstNameRole] = "firstName";
    roles[LastNameRole] = "lastName";
    roles[EmailRole] = "email";
    roles[CompanyRole] = "company";
    roles[ColorRole] = "color";
    roles[StatusRole] = "status";
    roles[IconRole] = "iconSource";
    roles[AnnotationsCountRole] = "annotations";
    return roles;
}

int ParticipantListModel::rowCount(const QModelIndex &parent) const
{
    return dataList.count();
}

QVariant ParticipantListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() > dataList.count())
        return QVariant();

    const ParticipantModel *contact = itemAt(index.row());
    if (role == FirstNameRole)
        return contact->firstName();
    else if (role == LastNameRole)
        return contact->lastName();
    else if (role == EmailRole)
        return contact->email();
    else if (role == CompanyRole)
        return contact->company();
    else if (role == IconRole)
        return contact->iconSource();
    else if (role == StatusRole)
        return  QVariant::fromValue(contact->status());
    else if (role == ColorRole)
        return  QVariant::fromValue(contact->color());
    else if (role == AnnotationsCountRole)
        return  contact->annotations();

    return QVariant();
}

Qt::ItemFlags ParticipantListModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if (index.row() != index.column())
        flags |= Qt::ItemIsEditable;
    return flags;
}

void ParticipantListModel::move(int oldIndex, int newIndex) {
    if (oldIndex < 0 || oldIndex >= rowCount() || newIndex < 0 || newIndex >= rowCount()) return;
    QModelIndex parent;
//    beginMoveRows(parent, oldIndex, oldIndex, parent, newIndex);
    beginResetModel();
    dataList.move(oldIndex, newIndex);
    endResetModel();
//    endMoveRows();
}

void ParticipantListModel::addContact(ParticipantModel* contact)
{
    addContact(contact->contactId(), contact->firstName(), contact->lastName(), contact->email(), contact->company(), contact->iconSource(),
               contact->color(), contact->status(), contact->annotations());
}

void ParticipantListModel::addContact(const int& contactId,
                                  const QString &firstName,
                                  const QString &lastName,
                                  const QString &email,
                                  const QString &company,
                                  const QString &iconSource,
                                  const QColor &color,
                                  const int &status,
                                  int annotations)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    ParticipantModel *contact = new ParticipantModel(contactId, firstName, lastName, email, company, iconSource, color, status);
    contact->setAnnotations(annotations);
    dataList.append(contact);
    endInsertRows();
}

void ParticipantListModel::addContact(const int& contactId,
                                  const QString &firstName,
                                  const QString &lastName,
                                  const QString &email,
                                  const QString &company,
                                  const QString &iconSource,
                                  const QColor &color)
{
    addContact(contactId, firstName, lastName, email, company, iconSource, color, 0, 0);
}

bool ParticipantListModel::removeContactAt(const int& index) {
    if (index < 0 || index >= rowCount()) return false;
    beginRemoveRows(QModelIndex(), index, index);
    ParticipantModel* contact = dataList.at(index);
    dataList.removeAt(index);
    endRemoveRows();

    delete contact;
    return true;
}

bool ParticipantListModel::editContactAt(const int& index) {
    if (index < 0 || index >= rowCount()) return false;
//    dataChanged(index, index);
    return true;
}
