#ifndef KSFINISHEDITDOCCONTROLLER_H
#define KSFINISHEDITDOCCONTROLLER_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <DocumentModel.h>
#include <ParticipantListModel.h>

class KSFinishEditDocController : public QObject
{
    Q_OBJECT

public:
    explicit KSFinishEditDocController(QObject* _parent = nullptr);
    ~KSFinishEditDocController();

    void fillData();
    Q_INVOKABLE void displayInitialState();

signals:
    void displayDocModel(QVariant docModel);
    void displayParticipants(QVariant participantsList);


public slots:
    void onDocumentNameChanged(QString newName);
    void onDocumentMessageChanged(QString newMessage);

private:
    DocumentModel m_doc_model;
    ParticipantListModel contactsModel;
};

#endif // KSFINISHEDITDOCCONTROLLER_H
