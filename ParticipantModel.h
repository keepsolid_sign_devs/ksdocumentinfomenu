#ifndef PARTICIPANTMODEL_H
#define PARTICIPANTMODEL_H

#include <QObject>
#include <QColor>

class ParticipantModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int userId READ userId WRITE setUserId NOTIFY userIdChanged)
    Q_PROPERTY(int contactId READ contactId WRITE setContactId NOTIFY contactIdChanged)
    Q_PROPERTY(QString firstName READ firstName WRITE setFirstName NOTIFY firstNameChanged)
    Q_PROPERTY(QString lastName READ lastName WRITE setLastName NOTIFY lastNameChanged)
    Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY emailChanged)
    Q_PROPERTY(QString company READ company WRITE setCompany NOTIFY companyChanged)
    Q_PROPERTY(QString iconSource READ iconSource WRITE setIconSource NOTIFY iconSourceChanged)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(int status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(int annotations READ annotations WRITE setAnnotations NOTIFY annotationsChanged)

public:
    ParticipantModel(QObject *parent=0);
    ParticipantModel(const int contactId, const QString &firstName, const QString &lastName, const QString &email, const QString &company,
                 const QString &iconSource, const QColor &color, QObject *parent=0);
    ParticipantModel(const int contactId, const QString &firstName, const QString &lastName, const QString &email, const QString &company,
                 const QString &iconSource, const QColor &color, const int status, QObject *parent=0);
    ParticipantModel(const int userId, const int contactId, const QString &firstName, const QString &lastName, const QString &email,
                     const QString &company, const QString &iconSource, const QColor &color, const int status, const int annotations, QObject *parent);
    ~ParticipantModel();

    enum ParticipantStatus: int {

        Signer = 0,
        Observer
    };
    Q_ENUMS(ParticipantStatus);

    int userId() const;
    int contactId() const;
    QString firstName() const;
    QString lastName() const;
    QString email() const;
    QString company() const;
    QString iconSource() const;
    QColor color() const;
    int status() const;
    int annotations() const;

    void setUserId(int userId);
    void setContactId(const int &contactId);
    void setFirstName(const QString &firstName);
    void setLastName(const QString &lastName);
    void setEmail(const QString &email);
    void setCompany(const QString &company);
    void setIconSource(const QString &iconSource);
    void setColor(const QColor &color);
    void setStatus(const int &status);
    void setAnnotations(int annotations);

public slots:

signals:
    void userIdChanged(int userId);
    void contactIdChanged();
    void firstNameChanged();
    void lastNameChanged();
    void emailChanged();
    void companyChanged();
    void iconSourceChanged();
    void colorChanged();
    void statusChanged();
    void annotationsChanged(int annotations);

private:
    int m_user_id;
    int m_contact_id;
    QString m_first_name;
    QString m_last_name;
    QString m_email;
    QString m_company;
    QString m_icon_source;
    QColor m_color;
    int m_status;
    int m_annotations;
};
#endif // PARTICIPANTMODEL_H
