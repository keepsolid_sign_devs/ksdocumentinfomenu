#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <KSDocumentInfoController.h>
#include <KSFinishEditDocController.h>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    qmlRegisterType<KSDocumentInfoController>("KSDocumentInfoController", 1, 0, "KSDocumentInfoController");
    qmlRegisterType<KSDocumentInfoController>("KSDocumentInfoButton", 1, 0, "KSDocumentInfoButton");
    qmlRegisterType<KSFinishEditDocController>("KSFinishEditDocController", 1, 0, "KSFinishEditDocController");
    qmlRegisterType<ParticipantModel>("com.documents", 1, 0, "ParticipantModel");
    qmlRegisterType<ParticipantListModel>("com.documents", 1, 0, "ParticipantListModel");
    qmlRegisterType<ParticipantListModel>("com.documents", 1, 0, "DocumentModel");

    KSFinishEditDocController *finishController = new KSFinishEditDocController;
    engine.rootContext()->setContextProperty("finishController", finishController);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
