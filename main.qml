import QtQuick 2.6
import QtQuick.Window 2.2
import "./KSDocumentInfoMenu"
import KSDocumentInfoController 1.0

Window {
    visible: true
    minimumWidth: 922
    minimumHeight: 730
    width: minimumWidth
    height: minimumHeight

    title: qsTr("Hello World")
    color: "lightgray"

    MouseArea {

        anchors.fill: parent
        onClicked: {

            documentInfoMenu.controller.openedStep = KSDocumentInfoController.NotChoosen;
            documentInfoMenu.controller.isMenuOpened = false;
        }
    }

    KSDocumentInfoMenu {

        id: documentInfoMenu
        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
        }
    }
    MouseArea {

        id: switchMenuState
        anchors {

            top: parent.top
            left: parent.left
        }
        height: 50
        width: height * 3
        onClicked: {

            documentInfoMenu.controller.incrementMenuState();
        }

        Text {

            anchors.fill: parent
            text: qsTr("Change menu state")

        }
    }
    MouseArea {

        id: switchhintIndex
        anchors {

            top: switchMenuState.bottom
            left: parent.left
        }
        height: 50
        width: height * 3
        onClicked: {

            documentInfoMenu.controller.incrementHintIndex();
        }

        Text {

            anchors.fill: parent
            text: qsTr("Change hint index")
        }
    }
}
