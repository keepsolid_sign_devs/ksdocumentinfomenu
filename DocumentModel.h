#ifndef DOCUMENTMODEL_H
#define DOCUMENTMODEL_H

#include <QObject>
#include <QDate>

class DocumentModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString docId READ docID)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(int status READ docStatus WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(QDate creationDate READ creationDate WRITE setCreationDate)
    Q_PROPERTY(QString message READ message WRITE setMessage NOTIFY messageChanged)
    Q_PROPERTY(bool hasOrder READ hasOrder WRITE setHasOrder NOTIFY hasOrderChanged)

public:
    explicit DocumentModel(QObject *parent = nullptr);

    QString docID() const;
    QString name() const;
    int docStatus() const;
    QDate creationDate() const;
    QString message() const;
    bool hasOrder() const;

    void setName(const QString name);
    void setStatus(const int status);
    void setCreationDate(const QDate creationDate);
    void setMessage(const QString message);
    void setHasOrder(bool hasOrder);

signals:
    void nameChanged(QString name);
    void statusChanged(int status);
    void messageChanged(QString message);
    void hasOrderChanged(bool hasOrder);

public slots:


private:
    QString m_docId;
    QString m_name;
    int m_status;
    QDate m_creationDate;
    QString m_message;
    bool m_hasOrder;
};

#endif // DOCUMENTMODEL_H
