#include <QDebug>
#include "ParticipantModel.h"

ParticipantModel::ParticipantModel(QObject *parent)
    : QObject(parent)
{
}

ParticipantModel::ParticipantModel(const int contactId, const QString &firstName, const QString &lastName, const QString &email,
                           const QString &company, const QString &iconSource, const QColor &color, QObject *parent)
    : QObject(parent), m_contact_id(contactId), m_first_name(firstName), m_last_name(lastName), m_email(email), m_company(company), m_icon_source(iconSource), m_color(color)
{
}

ParticipantModel::ParticipantModel(const int contactId, const QString &firstName, const QString &lastName, const QString &email,
                                   const QString &company, const QString &iconSource, const QColor &color, const int status, QObject *parent)
    : QObject(parent), m_contact_id(contactId), m_first_name(firstName), m_last_name(lastName), m_email(email), m_company(company), m_icon_source(iconSource), m_color(color), m_status(status)
{
}

ParticipantModel::ParticipantModel(const int userId, const int contactId,
                                   const QString &firstName, const QString &lastName, const QString &email, const QString &company,
                                   const QString &iconSource, const QColor &color, const int status, const int annotations, QObject *parent)
    : QObject(parent), m_user_id(userId), m_contact_id(contactId), m_first_name(firstName), m_last_name(lastName), m_email(email), m_company(company),
      m_icon_source(iconSource), m_color(color), m_status(status), m_annotations(annotations)
{

}

ParticipantModel::~ParticipantModel()
{
}

int ParticipantModel::userId() const{
    return m_user_id;
}

void ParticipantModel::setUserId(int userId)
{
    if (m_user_id == userId)
        return;

    m_user_id = userId;
    emit userIdChanged(m_user_id);
}

int ParticipantModel::contactId() const
{
    return m_contact_id;
}

void ParticipantModel::setContactId(const int &contactId)
{
    if (contactId != m_contact_id) {
        m_contact_id = contactId;
        emit contactIdChanged();
    }
}

QString ParticipantModel::firstName() const
{
    return m_first_name;
}

void ParticipantModel::setFirstName(const QString &firstName)
{
    if (firstName != m_first_name) {
        m_first_name = firstName;
        emit firstNameChanged();
    }
}

QString ParticipantModel::lastName() const
{
    return m_last_name;
}

void ParticipantModel::setLastName(const QString &lastName)
{
    if (lastName != m_last_name) {
        m_last_name = lastName;
        emit lastNameChanged();
    }
}

QString ParticipantModel::email() const
{
    return m_email;
}

void ParticipantModel::setEmail(const QString &email)
{
    if (email != m_email) {
        m_email = email;
        emit emailChanged();
    }
}

QString ParticipantModel::company() const
{
    return m_company;
}

void ParticipantModel::setCompany(const QString &company)
{
    if (company != m_company) {
        m_company = company;
        emit companyChanged();
    }
}

QString ParticipantModel::iconSource() const
{
    return m_icon_source;
}

int ParticipantModel::annotations() const
{
    return m_annotations;
}


void ParticipantModel::setIconSource(const QString &iconSource)
{
    if (iconSource != m_icon_source) {
        m_icon_source = iconSource;
        emit iconSourceChanged();
    }
}

QColor ParticipantModel::color() const
{
    return m_color;
}

void ParticipantModel::setColor(const QColor &color)
{
    if (color != m_color) {
        m_color = color;
        emit colorChanged();
    }
}

int ParticipantModel::status() const
{
    return m_status;
}

void ParticipantModel::setStatus(const int &status)
{
    if (status != m_status) {
        m_status = m_status;
        emit statusChanged();
    }
}

void ParticipantModel::setAnnotations(int annotations)
{
    if (m_annotations == annotations)
        return;

    m_annotations = annotations;
    emit annotationsChanged(m_annotations);
}
