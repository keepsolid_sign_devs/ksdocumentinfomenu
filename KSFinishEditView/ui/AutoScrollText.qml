import QtQuick 2.9
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.3

Flickable{
    id: textContainer

    property alias contentTextWidth: textContainer.contentWidth
    property alias contentTextHeight: textContainer.contentHeight
    property alias contentText: scrollingText
    property int  maxNotScrolledLines

    property color backdroundColor: "transparent"
    property color borderColor: scrollingText.activeFocus ? "#bfe5ff" : "#ebebeb"

    property alias animation: scrollAnimation
    property bool editable: false

    property string hintText: ""

    readonly property bool scrollbarNeeded: maxNotScrolledLines * fontMetrics.height < scrollingText.height

    property int  calculatedHeight: maxNotScrolledLines ? (maxNotScrolledLines * fontMetrics.height + 6 > scrollingText.paintedHeight ? scrollingText.height : maxNotScrolledLines * fontMetrics.height + 6) : parent.height

    height: maxNotScrolledLines ? calculatedHeight : parent.height
    boundsBehavior: Flickable.StopAtBounds

    function startAnimation(){
        scrollingText.y = 0
        scrollAnimation.start()
    }

    function stopAnimation(){
        scrollingText.y = 0
        scrollAnimation.stop()
    }

    flickableDirection: Flickable.VerticalFlick
    clip: true

    Rectangle{
        anchors.fill: parent
        color: backdroundColor
    }

    ScrollBar.vertical:  ScrollBar {
        id: scrollBar
        visible: scrollbarNeeded
    }

    FontMetrics {
        id: fontMetrics
        font.family: scrollingText.font.family
        font.pointSize: scrollingText.font.pointSize
    }

    TextEdit{
        id: scrollingText
        width: scrollingText.width
        wrapMode: Text.WordWrap
        textFormat: Text.StyledText
        anchors.right: parent.right
        anchors.left: parent.left
        font.family: "OpenSans"
        color: primaryTextColor
        font.pointSize: 12
        topPadding: 3
        bottomPadding: 3
        enabled: editable
        leftPadding: 10
        rightPadding: 6

        onCursorPositionChanged: {
            if(cursorPosition - cursorRectangle.height > textContainer.visibleArea.heightRatio * height + textContainer.contentY){
                textContainer.flick(0, -400)
            }

            if(cursorPosition - cursorRectangle.height  < textContainer.contentY){
                textContainer.flick(0, 400)
            }
        }

        Text {
            id: hint
            height: parent.height
            Layout.fillWidth: true
            Layout.fillHeight: true
            verticalAlignment: Text.AlignVCenter
            font.family: "OpenSans"
            color: "#8e8e8e"
            text: scrollingText.text!= "" ? "" : hintText
            anchors {
                right: parent.right
                left: parent.left
                top: parent.top; bottom: parent.bottom; rightMargin: 4; leftMargin: 8
            }
            font.pointSize: 12
            clip: true
            focus: false
        }


        NumberAnimation on y{
            id: scrollAnimation
            from: 0
            to: -1*scrollingText.height
            loops: Animation.Infinite
            running: false
            duration: 8000
        }
    }

}
