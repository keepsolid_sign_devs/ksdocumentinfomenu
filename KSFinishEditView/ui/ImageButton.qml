import QtQuick 2.7
import QtQuick.Controls 2.1

AbstractButton {
    property alias image: actionBtnIcon
    id: actionBtn
    width: 14
    height: 14

    Image {
        id: actionBtnIcon
        width: actionBtn.width
        height: actionBtn.width
        sourceSize.height: width
        sourceSize.width: height
        antialiasing: true
        anchors { verticalCenter: parent.verticalCenter; horizontalCenter: parent.horizontalCenter }
    }
}
