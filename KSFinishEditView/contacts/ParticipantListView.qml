import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.2
import com.documents 1.0
import "." as Contacts

Item {
    property ParticipantListModel participantsModel
    property ListView list
    property int scrollBarWidth: 8
    property bool colorIndication: false
    property bool dividerVisible: true
    property bool innerDragDrop: true
    property bool considerOrder: false
    property bool actionBtnVisibility: false
    property bool draggable: false
    property bool clickable: true
    property bool hoverEnabled: listView.hoverEnabled
    property alias visualModel: visualModel
    property alias dummyText: dummyText.text
    property alias dummyList: dummyList

    signal contactPositionChanged(int contactId, int position)
    signal actionBtnClicked(int contactId)

    signal dragPositionChanged(int y)
    signal dragStarted()
    signal dragFinished()
    signal itemClicked()

    id: rootLayout
    width: parent.width
    height: parent.height

    Item {
        id: dummyList
        visible: list.count < 1
        height: 55
        width: parent.width
        anchors { top: parent.top; topMargin: 7 }
        Image {
            id: dummyIcon
            anchors { right: parent.right; top: parent.top; rightMargin: 15 }
            width: 19
            height: width
            sourceSize.width: width
            sourceSize.height: width
        }

        Text {
            id: dummyText
            Layout.alignment: Qt.AlignLeft
            anchors { right: dummyIcon.left; left: parent.left; rightMargin: 4; top: dummyIcon.verticalCenter }

            Layout.fillWidth: true
            maximumLineCount: 3
            elide: Text.ElideRight
            wrapMode: Text.WordWrap
            textFormat: Text.PlainText
            clip: true
            color: "#444444"
            font.pointSize: 10
            font.family: "OpenSans"
        }

    }

    list: ListView {
        id: listView
        clip: true
        focus: true
        activeFocusOnTab: true
        parent: rootLayout
        visible: list.count > 0
        width: parent.width
        height: parent.height
        interactive: !draggable
        boundsBehavior: Flickable.DragAndOvershootBounds
        Component.onCompleted:{
            listView.forceActiveFocus()
        }
        //                snapMode: ListView.SnapOneItem
        spacing: 0
        model: visualModel
        highlight: highlight
        highlightFollowsCurrentItem: false
        highlightMoveVelocity: 1000
        onCurrentIndexChanged: {
            forceActiveFocus()
        }

        ScrollBar.vertical: ScrollBar {
            id:scrollBar
            parent: listView.parent
            hoverEnabled: true
            active: hovered
            width: scrollBarWidth
            contentItem: Rectangle {
                color: scrollBar.pressed ? "#2678c0" : "#2980cc"
            }
            background: Rectangle {
                color: "#ebebeb"
            }
            anchors.top: listView.top
            anchors.left: listView.right
            anchors.bottom: listView.bottom
            anchors.leftMargin: 10
        }
    }

    DelegateModel {
        id: visualModel
        model: participantsModel
        delegate: Component {
            Contacts.ParticipantDelegate {
                id: contactDelegate
                dropArea.visible: rootLayout.innerDragDrop && considerOrder
                colorIndication: rootLayout.colorIndication
                dividerVisible: rootLayout.dividerVisible
                actionButton.visible: actionBtnVisibility
                draggable: rootLayout.draggable
                clickable: rootLayout.clickable
            }
        }
    }

    Component {
        id: highlight
        Rectangle {
            visible: clickable
            width: parent.width; height: 45
            color: "#daebf9"
            border.color: "#332980cc"
            border.width: 1
            y: listView.currentItem.y + 5
            Behavior on y {
                SpringAnimation {
                    spring: 2
                    damping: 0.2
                }
            }
        }
    }
}
