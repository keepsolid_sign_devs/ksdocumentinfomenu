import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import "ui" as Ui
import "contacts" as Contacts
import com.documents 1.0
import KSFinishEditDocController 1.0

Item{
    id: root
    objectName: "FinishEditDocView"
    anchors.fill: parent

    readonly property color backgroundColor: "#f9f9f9"
    readonly property color primaryTextColor: "#444444"
    readonly property color secondaryTextColor: "#8e8e8e"

    function displayDocInfo(documentModel){
        console.log("displayDocInfo")
        docnameEditText.text = documentModel.name
        docnameEditText.textEdit.ensureVisible(0)
        docMessage.contentText.text = documentModel.message
        creationDate.text = Qt.formatDate(documentModel.creationDate, "d MMMM yyyy")
        participantsListView.considerOrder = documentModel.hasOrder
    }

    function displayParticipantsList(participants){
        console.log("displayParticipantsList")
        participantsListView.participantsModel = participants
    }

    Connections{
        target: finishController

        onDisplayDocModel:{
            displayDocInfo(docModel)
        }
        onDisplayParticipants:{
            displayParticipantsList(participantsList)
        }

    }

    Rectangle{
        anchors.fill: parent
        color: backgroundColor
    }

    ColumnLayout {
        spacing: 20
        anchors{top: parent.top; right: parent.right; left: parent.left; bottom: parent.bottom; rightMargin: 24; leftMargin: 24; bottomMargin: 20}

        Item{
            width: 0
            Layout.preferredHeight: 1
        }

        Item    {
            id: titleContainer
            width: parent.width
            height: mainTitle.height
            Layout.preferredHeight: mainTitle.height
            Layout.preferredWidth: parent.width

            Text{
                id: mainTitle
                text: "Final preparation"
                color: primaryTextColor
                font.family: "OpenSans"
                font.pointSize: 18
                wrapMode: Text.WordWrap
                anchors{ right: closeImg.left; left: parent.left; top: parent.top; rightMargin: 24;}
            }

            Image{
                id: closeImg
                width: 15
                height: 15
                sourceSize.width: width
                sourceSize.height: height
                anchors{top: parent.top; right: parent.right; }
                source: "icons/close_icon.png"
            }
        }

        ColumnLayout{
            width: parent.width
            Layout.fillWidth: true
            spacing: 7

            Text{
                text: "Document name"
                font.family: "OpenSans"
                font.pointSize: 12
                wrapMode: Text.WordWrap
                font.weight: Font.DemiBold
                color: primaryTextColor
                Layout.alignment: Qt.AlignLeft
            }

            Ui.InputField{
                id: docnameEditText
                hintText: "Enter document name"
                Layout.fillWidth: true
                textEdit.onTextChanged: {
                    finishController.onDocumentNameChanged(text)
                }
            }
        }

        RowLayout{
            width: parent.width
            Layout.preferredHeight: creationDate.paintedHeight
            Layout.preferredWidth: parent.width

            Text{
                anchors.left: parent.left
                text: "Created: "
                font.family: "OpenSans"
                font.pointSize: 12
                wrapMode: Text.WordWrap
                font.weight: Font.DemiBold
                color: primaryTextColor
                Layout.alignment:Qt.AlignLeft
            }

            Text{
                id: creationDate
                anchors.right: parent.right
                Layout.alignment: Qt.AlignRight
                text: "15 Jun 2017"
                font.family: "OpenSans"
                font.pointSize: 12
                wrapMode: Text.WordWrap
                font.weight: Font.DemiBold
                color: secondaryTextColor
            }
        }

        RowLayout{
            width: parent.width
            Layout.preferredHeight: docStatus.paintedHeight
            Layout.preferredWidth: parent.width

            Text{
                anchors.left: parent.left
                text: "Status:"
                font.family: "OpenSans"
                font.pointSize: 12
                wrapMode: Text.WordWrap
                font.weight: Font.DemiBold
                color: primaryTextColor
                Layout.alignment:Qt.AlignLeft
            }

            Text{
                id: docStatus
                anchors.right: parent.right
                text: "Draft"
                font.family: "OpenSans"
                font.pointSize: 12
                wrapMode: Text.WordWrap
                font.weight: Font.DemiBold
                color: secondaryTextColor
                Layout.alignment: Qt.AlignRight
            }
        }

        ColumnLayout{
            id: messsageContainer
            width: parent.width
            Layout.preferredWidth: parent.width
            spacing: 7

            Text{
                id: messageTitle
                text: "Message:"
                font.family: "OpenSans"
                font.pointSize: 12
                wrapMode: Text.WordWrap
                font.weight: Font.DemiBold
                color: primaryTextColor
                Layout.alignment: Qt.AlignLeft
            }

            Rectangle{
                width: parent.width
                Layout.fillWidth: true
                border.width: 1
                border.color: docMessage.borderColor
                height: docMessage.calculatedHeight + 2
                Layout.preferredHeight: height

                Ui.AutoScrollText{
                    id: docMessage
                    hintText: "Enter document message"
                    maxNotScrolledLines: 3
                    contentHeight: contentText.height
                    contentWidth: parent.width - scrollBar.width - 2
                    contentText.wrapMode: Text.WordWrap
                    contentText.color: primaryTextColor
                    contentText.font.pointSize: 10
                    backdroundColor: "white"
                    width: parent.width - 2
                    anchors.centerIn: parent
                    editable: true

                    contentText.onTextChanged: {
                        finishController.onDocumentMessageChanged(contentText.text)
                    }

                    ScrollBar.vertical: ScrollBar {
                        id:scrollBar
                        visible: docMessage.scrollbarNeeded
                        hoverEnabled: true
                        active: hovered
                        width: 6
                        contentItem: Rectangle {
                            color: scrollBar.pressed ? "#2678c0" : "#2980cc"
                        }
                        background: Rectangle {
                            color: "#ebebeb"
                        }
                        anchors.top: docMessage.top
                        anchors.right: docMessage.right
                        anchors.bottom: docMessage.bottom
                        anchors.leftMargin: 10
                    }
                }
            }
        }

        ColumnLayout{
            width: parent.width
            Layout.preferredWidth: parent.width
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignBottom
            spacing: 7

            Text{
                text: "Document participants:"
                font.family: "OpenSans"
                font.pointSize: 12
                wrapMode: Text.WordWrap
                font.weight: Font.DemiBold
                color: primaryTextColor
                Layout.alignment: Qt.AlignLeft
            }

            Contacts.ParticipantListView{
                id: participantsListView
                width: parent.width
                dividerVisible: false
                innerDragDrop: false
                scrollBarWidth: 10
                hoverEnabled: false
                draggable: false
                clickable: false
                considerOrder: true
                colorIndication: true
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }
}
