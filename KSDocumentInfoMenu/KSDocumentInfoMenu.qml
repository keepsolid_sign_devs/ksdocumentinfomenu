import QtQuick 2.0
import KSDocumentInfoController 1.0
import KSDocumentInfoButton 1.0

Item {
    objectName: "DocInfoMenu"
    id: documentInfoMenu
    property real openedMenuWidth: stepsView.width + docMenuLoader.width + separator.width
    property real closedMenuWidth: stepsView.width
    property alias controller: documentInfoViewController

    anchors {
        top: parent.top
        bottom: parent.bottom
        right: parent.right
    }
    width: documentInfoViewController.isMenuOpened ? openedMenuWidth : closedMenuWidth;

    Behavior on width {

        //PropertyAnimation {
        NumberAnimation {

            id: widthAnimation
            target: documentInfoMenu
            property: "width"
            easing.type: Easing.OutQuint
            duration: 500
        }
    }

    Loader {
        id: docMenuLoader
        objectName: "docMenuLoader"
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: stepsView.right
            leftMargin: 1
        }
        width: 254
        source: "../ParticipantsMenu.qml"

        KSSendButton {

            id: sendButton
            anchors {

                left: parent.left
                leftMargin: 18
                right: parent.right
                rightMargin: 18
                bottom: parent.bottom
            }

            height: 36
        }
    }

    Rectangle {

        id: separator
        anchors {

            top: parent.top
            bottom: parent.bottom
            left: stepsView.right
            right: docMenuLoader.left
        }
        color: "#dbdbdb"
    }

    KSDocumentInfoStepsView {

        id: stepsView
        anchors {

            top: parent.top
            bottom: parent.bottom
            left: parent.left
        }
    }

    KSDocumentInfoController {
        id: documentInfoViewController
        onOpenedStepChanged: {
            stepsView.updateButtonsState();
        }
        onHintStepChanged: {
            stepsView.updateButtonsState();
        }
        Component.onCompleted: {
            stepsView.updateButtonsState();
        }
    }

    Connections {
        target: documentInfoViewController
        onParticipantsMenuOpened: {
            console.log("showParticipantsMenu")
            docMenuLoader.setSource("../ParticipantsMenu.qml")
        }
        onAnnotationsMenuOpened:{
            console.log("showAnnotationsMenu")
            docMenuLoader.setSource("../AnnotationsMenu.qml")
        }
        onFinishMenuOpened:{
            console.log("showFinishMenu")
            docMenuLoader.setSource("../KSFinishEditView/KSFinishEditView.qml")
            finishController.displayInitialState();
        }
    }
}
