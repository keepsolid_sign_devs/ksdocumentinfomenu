import QtQuick 2.0
import KSDocumentInfoController 1.0

Item {

    id: root
    visible: {

        anchors.bottomMargin = -height;
        switch (documentInfoViewController.openedStep) {

        case KSDocumentInfoController.NotChoosen:

            return false;
            break;
        case KSDocumentInfoController.Participants:

            if (documentInfoViewController.hintStep == KSDocumentInfoController.Annotations) {

                anchors.bottomMargin = 18;
                return true;
            }
            return false;
            break;
        case KSDocumentInfoController.Annotations:

            if (documentInfoViewController.hintStep == KSDocumentInfoController.Finish) {

                anchors.bottomMargin = 18;
                return true;
            }
            return false;
            break;
        case KSDocumentInfoController.Finish:

            if (documentInfoViewController.hintStep == KSDocumentInfoController.Send) {

                anchors.bottomMargin = 18;
                return true;
            }
            return false;
            break;
        default:

            return false;
            break;
        }
    }

    Rectangle {

        anchors.fill: parent

        KSButtonGradient {

            anchors.fill: parent
        }

        Text {

            anchors.fill: parent
            color: "#ffffff"
            clip: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family: "OpenSans"
            font.pixelSize: 13
            text: documentInfoViewController.hintStep == KSDocumentInfoController.Send ? qsTr("Send") : qsTr("Next step")
        }
    }

    MouseArea {

        anchors.fill: parent
        onClicked: {

        }
    }
}
