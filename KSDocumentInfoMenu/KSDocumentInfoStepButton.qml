import QtQuick 2.0
import QtQml 2.2
import QtGraphicalEffects 1.0
import KSDocumentInfoController 1.0

Item {

    id: documentInfoButton

    property int buttonIndex: KSDocumentInfoController.NotChoosen
    width: 70
    height: 70
    state: "NotActive"

    states: [

        State {
            name: "Active"
            PropertyChanges {
                target: stepDescription;
                color: "#ffffff"
            }
            PropertyChanges {
                target: stepImage;
                source: buttonImage();
            }
        },
        State {
            name: "NotActive"
            PropertyChanges {
                target: stepDescription;
                color: "#43454e"
            }
            PropertyChanges {
                target: stepImage;
                source: buttonImage();
            }
        }
    ]

    transitions: [

        Transition {

            SequentialAnimation {

                PropertyAnimation {

                    targets: [stepImage, stepDescription];
                    properties: "opacity";
                    to: 0.1
                    duration: 150
                }
                PropertyAnimation {

                    targets: [stepImage, stepDescription];
                    properties: "opacity";
                    to: 1;
                    duration: 150;
                }
            }
        }
    ]

    Rectangle {
        id: rectangle1
        width: 70
        height: 70
        color: "#00000000"
        opacity: 1
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Text {

            id: stepDescription
            height: 14
            font.wordSpacing: 0
            font.pixelSize: 10
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 8
            text: buttonText();
        }

        Item {

            id: imagePad
            anchors.margins: 5
            anchors {

                top: parent.top
                bottom: stepDescription.bottom
                left: parent.left
                right: parent.right
            }

            Image {

                id: stepImage
                width: stepImage.sourceSize.width / 2
                height: stepImage.sourceSize.height / 2
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                fillMode: Image.PreserveAspectFit
            }
        }
        MouseArea {

            anchors.fill: parent
            onClicked: {

                documentInfoViewController.stepButtonClicked(buttonIndex);
            }
        }
    }

    function buttonText() {

        switch (buttonIndex) {
        case KSDocumentInfoController.Participants:
            return qsTr("Participants");
            break;
        case KSDocumentInfoController.Annotations:
            return qsTr("Annotations");
            break;
        case KSDocumentInfoController.Finish:
            return qsTr("Finish");
            break;
        case KSDocumentInfoController.Send:
            return qsTr("Send");
            break;
        default:
            break;
        }
        return "";
    }

    function buttonImage() {

        switch (buttonIndex) {
        case KSDocumentInfoController.Participants:
            if (state == "NotActive") {

                return "Assets/IconParticipantsNormal.png";
            } else {

                return "Assets/IconParticipantsActive.png";
            }
            break;
        case KSDocumentInfoController.Annotations:
            if (state == "NotActive") {

                return "Assets/IconAnnotationsNormal.png";
            } else {

                return "Assets/IconAnnotationsActive.png";
            }
            break;
        case KSDocumentInfoController.Finish:
            if (state == "NotActive") {

                return "Assets/IconFinishNormal.png";
            } else {

                return "Assets/IconFinishActive.png";
            }
            break;
        case KSDocumentInfoController.Send:

            return "Assets/IconSendActive.png";
            break;
        default:
            break;
        }
        return "";
    }
}
