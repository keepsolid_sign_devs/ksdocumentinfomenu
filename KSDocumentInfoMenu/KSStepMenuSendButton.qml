import QtQuick 2.0
import KSDocumentInfoController 1.0

Item {

    id: root
    state: documentInfoViewController.hintStep == KSDocumentInfoController.Send && !documentInfoViewController.isMenuOpened ? "Opened" : "Closed"

    states: [

        State {
            name: "Opened"
            PropertyChanges {

                target: root
                opacity: 1
            }
        },
        State {
            name: "Closed"
            PropertyChanges {

                target: root
                opacity: 0
            }
        }
    ]

    transitions: Transition {

            PropertyAnimation {

                properties: "opacity"
                easing.type: Easing.Linear
                duration: 200
            }
        }

    KSStepButton {

        id: gradientOverlay
        width: 80
        height: 80
        anchors.centerIn: parent
        selectedButtonIndex: KSDocumentInfoController.Send
    }

    KSDocumentInfoStepButton {

        anchors.fill: parent
        buttonIndex: KSDocumentInfoController.Send
        state: "Active"
    }
}
