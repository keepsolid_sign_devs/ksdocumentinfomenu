import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {

    property color menuClosedBottonGradientColor: "#2168a6"
    property color menuClosedTopGradientColor: "#33a0ff"
    property color menuOpenedBottonGradientColor: "#2678c0"
    property color menuOpenedTopGradientColor: "#2b88d9"

    anchors.fill: parent

    RectangularGlow {

        id: shadowEffect
        anchors.fill: parent
        color: "#972f82c9"
        glowRadius: 34
    }

    LinearGradient {

        id: activeGradient
        anchors.fill: parent
        start: Qt.point(x, y);
        end: Qt.point(width, height);
        gradient: documentInfoViewController.isMenuOpened ? openedGradient : closedGradient;
    }

    Gradient {

        id: closedGradient
        GradientStop {
            position: 0
            color: menuClosedTopGradientColor
        }

        GradientStop {
            position: 1
            color: menuClosedBottonGradientColor
        }
    }

    Gradient {

        id: openedGradient
        GradientStop {
            position: 0
            color: menuOpenedTopGradientColor
        }

        GradientStop {
            position: 1
            color: menuOpenedBottonGradientColor
        }
    }
}
