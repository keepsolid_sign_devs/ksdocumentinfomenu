import QtQuick 2.0
import KSDocumentInfoController 1.0

Item {

    property int selectedButtonIndex: KSDocumentInfoController.NotChoosen

    Behavior on y {

        NumberAnimation {
            //This specifies how long the animation takes
            duration: 200
            //This selects an easing curve to interpolate with, the default is Easing.Linear
            easing.type: Easing.InOutQuad
        }
    }

    Rectangle {

        id: activeRectangle
        anchors.fill: parent

        KSButtonGradient {

            anchors.fill: parent
        }

        Text {
            id: stepNumber
            width: 20
            height: 55
            color: "#19ffffff"
            clip: true
            verticalAlignment: Text.AlignBottom
            anchors.right: parent.right
            anchors.top: parent.top
            font.family: "Times New Roman"
            font.pixelSize: 54
            text: selectedButtonIndex
            horizontalAlignment: Text.AlignHCenter
            visible: selectedButtonIndex != KSDocumentInfoController.Send && documentInfoViewController.menuState == KSDocumentInfoController.Prepeared
        }
    }
}
