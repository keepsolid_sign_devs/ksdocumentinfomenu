import QtQuick 2.0
import KSDocumentInfoController 1.0

Item {

    id: documentInfoStepsView
    implicitWidth: 70
    implicitHeight: 380

    Rectangle {

        id: bacgroundRectangle
        anchors.fill: parent
        color: "#f4f4f4"

        Rectangle {

            id: imageRectangle
            width: 50
            height: 50
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 20
            radius: width / 2
            clip: true
            color: "blue"
            border.color: "#ffffff"
            border.width: 2
        }

        KSStepButton {

            id: gradientOverlay
            width: 80
            height: 80
            anchors.horizontalCenter: parent.horizontalCenter
            selectedButtonIndex: indexToSelect();
            visible: {


                return (gradientOverlay.selectedButtonIndex != KSDocumentInfoController.NotChoosen &&
                        gradientOverlay.selectedButtonIndex != KSDocumentInfoController.Send)
            }
        }

        Column {

            id: column
            anchors {

                top: imageRectangle.bottom
                topMargin: 20
                horizontalCenter: parent.horizontalCenter
                bottom: sendButton.bottom
            }

            Repeater {

                id: buttonsRepeater
                model: {

                    switch (documentInfoViewController.menuState) {

                    case KSDocumentInfoController.Prepeared:

                        return 3;//.Participants && .Annotations && .Finish
                        break;
                    case KSDocumentInfoController.Signing:

                        return 2;//.Annotation && .Finish
                        break;
                    case KSDocumentInfoController.Signed:

                        return 1;//just .Finish
                        break;
                    default:
                        break;
                    }
                }
                KSDocumentInfoStepButton {

                    height: 70
                    width: 70
                    buttonIndex: {

                        switch (documentInfoViewController.menuState) {

                        case KSDocumentInfoController.Prepeared:

                            return model.index + 1;//0 is .NotChoosen enum state
                            break;
                        case KSDocumentInfoController.Signing:

                            return model.index + 2;//.Participants isn't needed in this case
                            break;
                        case KSDocumentInfoController.Signed:

                            return model.index + 3;//.Participants && .Annotations isn't needed in this case
                            break;
                        default:
                            break;
                        }
                    }
                }
            }
        }

        KSStepMenuSendButton {

            id: sendButton
            anchors {

                bottom: parent.bottom;
                left: parent.left;
            }
            width: 70
            height: 70
        }
    }

    function updateButtonsState() {

        var activeButton = buttonForActiveIndex();
        for (var i = 0; i < buttonsRepeater.model; i++) {

            var button = buttonsRepeater.itemAt(i);
            if (!activeButton || button.buttonIndex != activeButton.buttonIndex) {

                button.state = "NotActive";
            } else {

                button.state = "Active";
                gradientOverlay.y = (button.y + button.height / 2 + column.y) - gradientOverlay.width / 2
            }
        }
    }

    function buttonForActiveIndex() {

        var index;
        switch (documentInfoViewController.menuState) {

        case KSDocumentInfoController.Prepeared:

            index = indexToSelect() - 1;//.Participants && .Annotations && .Finish
            break;
        case KSDocumentInfoController.Signing:

            index = indexToSelect() - 2;//.Annotation && .Finish
            break;
        case KSDocumentInfoController.Signed:

            index = indexToSelect() - 3;//just .Finish
            break;
        default:
            break;
        }
        var button = buttonsRepeater.itemAt(index);
        return button;
    }

    function indexToSelect() {

        var indexToSelect;
        if (documentInfoViewController.openedStep == KSDocumentInfoController.NotChoosen) {

            indexToSelect = documentInfoViewController.hintStep;
        } else {

            indexToSelect = documentInfoViewController.openedStep;
        }
        return indexToSelect;
    }
}
