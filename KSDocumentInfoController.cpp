#include "KSDocumentInfoController.h"
#include <QDebug>
#include <QQuickItem>

KSDocumentInfoController::KSDocumentInfoController(QObject *parent) : QObject(parent)
{
    _hintStepIndex = Send;
    _menuState = Prepeared;
}

KSDocumentInfoController::~KSDocumentInfoController(){
}

void KSDocumentInfoController::stepButtonClicked(KSDocumentInfoButton index) {
    if (index != _openedStepIndex) {
        _openedStepIndex = index;
        emit openedStepChanged();
    }
    if (!_isMenuOpened) {

        _isMenuOpened = true;
        emit menuOpenedChanged();
    }

    switch (index) {
    case Participants:
        qDebug()<<"participantsMenuOpened";
        emit participantsMenuOpened();
        break;
    case Annotations:
        qDebug()<<"annotationsMenuOpened";
        emit annotationsMenuOpened();
        break;
    case Finish:
        qDebug()<<"finishMenuOpened";
        emit finishMenuOpened();
        break;
    }
}


void KSDocumentInfoController::incrementMenuState() {

    KSDocumentInfoMenuState state = _menuState;

    switch (state) {
    case Prepeared:
        _menuState = Signing;
        _hintStepIndex = Annotations;
        break;
    case Signing:
        _menuState = Signed;
        _hintStepIndex = Finish;
        break;
    case Signed:
        _menuState = Prepeared;
        _hintStepIndex = Participants;
        break;
    default:
        break;
    }
    emit menuStateChanged();
    emit hintStepChanged();
}

void KSDocumentInfoController::incrementHintIndex() {

    KSDocumentInfoButton index = _hintStepIndex;

    switch (index) {
    case NotChoosen:
        _hintStepIndex = Participants;
        break;
    case Participants:
        _hintStepIndex = Annotations;
        break;
    case Annotations:
        _hintStepIndex = Finish;
        break;
    case Finish:
        _hintStepIndex = Send;
        break;
    case Send:
        _hintStepIndex = NotChoosen;
        break;
    default:
        break;
    }
    if (_menuState != Prepeared) {
        _hintStepIndex = NotChoosen;
    }
    emit hintStepChanged();
}
